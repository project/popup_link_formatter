<?php

namespace Drupal\popup_link_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "popup_link",
 *   label = @Translation("Popup link"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class PopupLinkFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'popup_link' => '',
      'popup_progress_type' => '_none',
      'popup_dialog_type' => 'modal',
      'popup_width' => 50,
      'popup_min_height' => 50,
      'popup_css_class' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['popup_link'] = [
      '#type' => 'checkbox',
      '#title' => t('Popup Link'),
      '#default_value' => $this->getSetting('popup_link'),
      '#description' => t('Show a content link in a popup.'),
    ];

    $elements['popup_progress_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Progress'),
      '#options' => [
        '_none' => $this->t('None'),
        'bar' => $this->t('Bar'),
        'throbber' => $this->t('Throbber'),
        'fullscreen' => $this->t('Fullscreen'),
      ],
      '#default_value' => $this->getSetting('popup_progress_type'),
      '#states' => [
        'visible' => [
          [':input[name="fields[field_popup_link_link][settings_edit_form][settings][popup_link]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $elements['popup_dialog_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Dialog type'),
      '#options' => [
        'modal' => $this->t('Modal'),
        'dialog' => $this->t('Dialog'),
      ],
      '#default_value' => $this->getSetting('popup_dialog_type'),
      '#states' => [
        'visible' => [
          [':input[name="fields[field_popup_link_link][settings_edit_form][settings][popup_link]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $elements['popup_width'] = [
      '#type' => 'number',
      '#min' => 20,
      '#max' => 100,
      '#default_value' => $this->getSetting('popup_width') ?: 50,
      '#title' => $this->t('Width'),
      '#placeholder' => $this->t('Width'),
      '#field_suffix' => '%',
      '#states' => [
        'visible' => [
          [':input[name="fields[field_popup_link_link][settings_edit_form][settings][popup_link]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $elements['popup_min_height'] = [
      '#type' => 'number',
      '#min' => 20,
      '#max' => 100,
      '#default_value' => $this->getSetting('popup_min_height') ?: 50,
      '#title' => $this->t('Minimun height'),
      '#placeholder' => $this->t('Height'),
      '#field_suffix' => '%',
      '#states' => [
        'visible' => [
          [':input[name="fields[field_popup_link_link][settings_edit_form][settings][popup_link]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $elements['popup_css_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS class'),
      '#maxlength' => 255,
      '#default_value' => $this->getSetting('popup_css_class'),
      '#states' => [
        'visible' => [
          [':input[name="fields[field_popup_link_link][settings_edit_form][settings][popup_link]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = parent::viewElements($items, $langcode);
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {

      if (!empty($settings['popup_link'])) {

        $element[$delta]['#attributes'] = [
          'class' => 'use-ajax ' . $settings["popup_css_class"],
          'data-dialog-type' => $settings["popup_dialog_type"],
          'data-dialog-options' => Json::encode([
            'width' => $settings["popup_width"] . '%',
            'minHeight' => $settings["popup_min_height"] . '%',
          ]),
        ];

        if ($settings["popup_progress_type"] != '_none') {
          $element[$delta]['#attributes']['data-dialog-progress'] = ['type' => $settings["popup_progress_type"]];
        }

        $element[$delta]['#attached']['library'][] = 'core/drupal.ajax';
        $element[$delta]['#attached']['library'][] = 'core/drupal.dialog';
        $element[$delta]['#attached']['library'][] = 'core/drupal.dialog.ajax';
      }
    }
    return $element;
  }

}
