CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

The Popup Link Formatter module allows links to be open in a modal or dialog
popup.


REQUIREMENTS
------------

This module requires no additional modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. The configuration is done on a
field-by-field basis on the "Manage display" page by selecting the *Popup link*
formatter.
